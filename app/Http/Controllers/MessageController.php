<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Message;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{
    public function showMessage($id)
    {
        $messages = Message::where('sender_id','=', $id)->orWhere('recevier_id','=', $id)->get();    
        $member = User::find($id);
        return view('members.message', compact('messages' , 'member'));

    }

    public function sendMessage(Request $request)
    {
        // dd();
        $input = $request->all();
        unset($input['_token']);
        $message = Message::create($input);
        return redirect('messages/'.$request->recevier_id);
    }
}
