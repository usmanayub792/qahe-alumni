@extends('layouts.sidebar')

@section('dashboard')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Members</div>

                <div class="card-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                @foreach ($members as $member)
                                    <div class="row g-0">
                                        <div class="col-md-3">
                                            <img src="@if($member->image){{ '../storage/images/'.$member->image }}@else{{ asset('img/no-image.jpg') }}@endif" alt="..." class="img-fluid">
                                        </div>
                                        <div class="col-md-9">
                                            <div class="card-body">
                                                <h5 class="card-title">{{$member->name}}</h5>
                                                <p class="card-text">{{$member->profession}}</p>
                                                <p class="card-text">{{$member->bio}}</p>
                                                <a href="{{ route('messages', $member->id)}}" class="float-right btn btn-success">Send Message</a>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
@endsection
   