@extends('layouts.sidebar')

@section('dashboard')

    <style>
        .sender{
            color: white;
            padding: 10px;
            border-radius: 10px;
        }
        .receiver{
            background: #d0d6d6;
            padding: 10px;
            border-radius: 10px; 
        }
    </style>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Members</div>

                <div class="card-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-4" style="border-right:2px solid">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <img src="@if($member->image){{ '../storage/images/'.$member->image }}@else{{ asset('img/no-image.jpg') }}@endif" alt="..." class="img-fluid">
                                                </div>
                                                <div class="col-md-6">
                                                    <br>
                                                    <h4>{{$member->name}}</h4>
                                                    <h5>{{$member->profession}}</h5>
                                                    <p>{{ Str::limit($member->bio, 50)}}</p>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                @foreach ($messages as $message)
                                                    @if(Auth::id() == $message->sender_id)
                                                    <div class="offset-md-1"></div>
                                                    <div class="col-md-10">
                                                        <p class="float-right bg-primary sender">
                                                            {{$message->message}} 
                                                        </p>
                                                    </div>
                                                    <div class="offset-md-1"></div>
                                                    @else
                                                    <div class="offset-md-1"></div>
                                                    <div class="col-md-10">
                                                        <p class="float-left bg-default receiver">
                                                            {{$message->message}} 
                                                        </p>
                                                    </div>
                                                    <div class="offset-md-1"></div>
                                                    @endif
                                                @endforeach
                                                <div class="col-md-12">
                                                    <form action="{{ route('send.message') }}" method="post">
                                                        @csrf
                                                        <textarea name="message" class="form-control" cols="10" rows="3"></textarea>
                                                        <input type="hidden" name="sender_id" value="{{Auth::id()}}">
                                                        <input type="hidden" name="recevier_id" value="{{$member->id}}">
                                                        <input type="submit" value="Send Message" class="btn btn-success btn-lg btn-block">
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
@endsection
   