@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-2 collapse show d-md-flex bg-light pt-2 pl-0 min-vh-100" id="sidebar">
            <ul class="nav flex-column flex-nowrap overflow-hidden" style="width:100%">
                {{-- <li class="nav-item btn">
                    <a class="nav-link text-truncate btn btn-primary" href="{{ route('home') }}"><i
                            class="fa fa-home"></i> <span class="d-none d-sm-inline">Dashboard</span></a>
                </li> --}}
                <!-- <li class="nav-item">
                    <a class="nav-link collapsed text-truncate" href="#submenu1" data-toggle="collapse" data-target="#submenu1"><i class="fa fa-table"></i> <span class="d-none d-sm-inline">Reports</span></a>
                    <div class="collapse" id="submenu1" aria-expanded="false">
                        <ul class="flex-column pl-2 nav">
                            <li class="nav-item"><a class="nav-link py-0" href="#"><span>Orders</span></a></li>
                            <li class="nav-item">
                                <a class="nav-link collapsed py-1" href="#submenu1sub1" data-toggle="collapse" data-target="#submenu1sub1"><span>Customers</span></a>
                                <div class="collapse" id="submenu1sub1" aria-expanded="false">
                                    <ul class="flex-column nav pl-4">
                                        <li class="nav-item">
                                            <a class="nav-link p-1" href="#">
                                                <i class="fa fa-fw fa-clock-o"></i> Daily </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link p-1" href="#">
                                                <i class="fa fa-fw fa-dashboard"></i> Dashboard </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link p-1" href="#">
                                                <i class="fa fa-fw fa-bar-chart"></i> Charts </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link p-1" href="#">
                                                <i class="fa fa-fw fa-compass"></i> Areas </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li> -->
                <li class="nav-item btn"><a class="nav-link text-truncate btn btn-primary" href="{{ route('event.new') }}"><i
                            class="fa fa-bar-chart"></i> <span class="d-none d-sm-inline">Future Events</span></a></li>
                <li class="nav-item btn"><a class="nav-link text-truncate btn btn-primary" href="{{ route('event.past') }}"><i
                                class="fa fa-bar-chart"></i> <span class="d-none d-sm-inline">Past Events</span></a></li>
                <li class="nav-item btn"><a class="nav-link text-truncate btn btn-primary" href="{{ route('user.index') }}"><i
                            class="fa fa-download"></i> <span class="d-none d-sm-inline">Members</span></a></li>
            </ul>
        </div>
        <div class="col pt-2">
            @yield('dashboard')
        </div>
    </div>
</div>
@endsection