@extends('layouts.sidebar')

@section('dashboard')
<div class="container">
    <style>
        .upload {
            position: relative;
            overflow: hidden;
        }

        .upload>input {
            position: absolute;
            font-size: 50px;
            opacity: 0;
            right: 0;
            top: 0;
        }
    </style>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Profile</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    @isset($status)
                        <div id="msg" class="alert alert-success" role="alert">
                            {{ $status }}
                        </div>
                    @endisset                    
                    <form action="{{route('user-update', $user->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <img src="@if($user->image){{ '../storage/images/'.$user->image }}@else {{ asset('img/no-image.jpg') }} @endif" width="40%" heigth="40%" alt="No Image" class="col-md-5 img-fluid">
                                        <br><br>
                                        <div class="col-md-5 btn btn-primary upload">
                                            Upload
                                            <input type="file" name="image" class="form-control-file">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="userName">Name</label>
                                        <input type="text" class="form-control" value="@if($user->name){{$user->name}} @endif" name="name" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label for="userphone">Phone</label>
                                        <input type="text" class="form-control"  value="@if($user->phone){{$user->phone}} @endif" name="phone" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label for="userEmail">Email</label>
                                        <input type="email" class="form-control"  value="@if($user->email){{$user->email}} @endif" name="email" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="userprofession">Profession</label>
                                        <input type="text" class="form-control"  value="@if($user->profession){{$user->profession}} @endif" name="profession" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="userAddress">Address</label>
                                        <input type="text" class="form-control"  value="@if($user->address){{$user->address}} @endif" name="address" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="userBio">bio</label>
                                        <textarea name="bio" class="form-control" cols="10" rows="3">@if($user->bio){{$user->bio}}@endif</textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <input type="submit" value="Update Profile" class="btn btn-success" name="submit">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form action="{{ route('deleteUser', $user->id) }}" method="post">
                        @csrf
                    {{-- <a href="" class="btn btn-danger "></a> --}}
                        <input type="submit" value="Delete My Account" class="btn btn-danger float-right">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $(".alert-success").fadeOut(3000);
    });

    // $('.delete-account').click(function(){
    //     let check = confirm('Are you sure you want to delete you account');

    //     if(check)
    //     {
            
    //     }
    // })
    
</script>
@endsection