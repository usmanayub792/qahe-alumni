@extends('layouts.sidebar')

@section('dashboard')
<div class="container">
    <style>
        .upload {
            position: relative;
            overflow: hidden;
        }

        .upload>input {
            position: absolute;
            font-size: 50px;
            opacity: 0;
            right: 0;
            top: 0;
        }
    </style>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Create Event</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    @isset($status)
                        <div id="msg" class="alert alert-success" role="alert">
                            {{ $status }}
                        </div>
                    @endisset
                    <form action="{{route('event.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="offset-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="name">Event Name</label>
                                        <input type="text" class="form-control" name="name" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="date">Event Date</label>
                                        <input type="datetime-local" class="form-control" name="date" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="description">Description</label>
                                        <textarea name="description" class="form-control" cols="10" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <div class="col-md-5 btn btn-primary upload">
                                            Upload
                                            <input type="file" name="image" class="form-control-file">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <input type="submit" value="Create Event" class="btn btn-success" name="submit">
                                    </div>
                                </div>
                            </div>
                            <div class="offset-md-3"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $(".alert-success").fadeOut(3000);
    });
</script>
@endsection