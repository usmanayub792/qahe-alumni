@extends('layouts.sidebar')

@section('dashboard')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ $event->name}}</div>

                <div class="card-body">
                    
                    <div class="container">
                        <a href="{{url()->previous()}}" class="btn btn-primary">Go Back</a> <br><br>
                        <div class="row">
                            <div class="col-md-12">
                                <img src="@if($event->image){{ '../storage/images/'.$event->image }}@else{{ asset('img/no-image.jpg') }}@endif" style="height:200px" alt="" class="img-fluid">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <h1>{{ $event->name}}</h1>
                                <p>{{$event->date}}</p>
                                <p>{{ $event->description}}</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
