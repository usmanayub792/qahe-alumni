@extends('layouts.sidebar')

@section('dashboard')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Past Events</div>

                <div class="card-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <a href="{{route('event.create')}}" class="btn btn-primary">Create Event</a>
                                <br>
                                <br>
                                @foreach ($events as $event)
                                    <div class="row g-0">
                                        <a href="{{ route('event.show', $event->id)}}" style="display: inherit; color: black;">
                                            <div class="col-md-3">
                                                <img src="@if($event->image){{ '../storage/images/'.$event->image }}@else{{ asset('img/no-image.jpg') }}@endif" alt="..." class="img-fluid">
                                            </div>
                                            <div class="col-md-9">
                                                <div class="card-body">
                                                    <h5 class="card-title">{{$event->name}}</h5>
                                                    <p class="card-text">{{$event->date}}</p>
                                                    <p class="card-text">{{$event->description}}</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <hr>
                                @endforeach
                            </div>
                        </div>
                        <div class="d-flex justify-content-center">
                            {!! $events->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
@endsection
   