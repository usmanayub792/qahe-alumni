<?php

use App\Http\Controllers\MessageController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'UserController@profile')->name('profile');
Route::post('update/user/{id}', 'UserController@update')->name('user-update');
Route::resource('user', 'UserController');
Route::resource('event', 'EventController');
Route::get('future/event', 'EventController@new')->name('event.new');
Route::get('past/event', 'EventController@past')->name('event.past');
Route::get('messages/{id}', 'MessageController@showMessage')->name('messages');
Route::post('send/message', 'MessageController@sendMessage')->name('send.message');
Route::post('user/{id}', 'UserController@deleteUser')->name('deleteUser');
